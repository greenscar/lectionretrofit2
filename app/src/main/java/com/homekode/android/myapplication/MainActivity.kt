package com.homekode.android.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        list_w.layoutManager = LinearLayoutManager(applicationContext)


        var retrofit = Retrofit.Builder()
                .baseUrl("http://icomms.ru/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        var service = retrofit.create(APIService::class.java)

        var result = service.getWeather(24)

        result.enqueue(object : Callback<List<Weather>> {
            override fun onFailure(call: Call<List<Weather>>?, t: Throwable?) {

            }
            override fun onResponse(call: Call<List<Weather>>?, response: Response<List<Weather>>?) {
                list_w.adapter = MyAdapter(ArrayList(response!!.body()))
            }

        })


    }
}
