package com.homekode.android.myapplication

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_list.view.*

/**
 * Created by ASUS on 05.02.2018.
 */
class MyAdapter(var data:List<Weather>) :  RecyclerView.Adapter<MyAdapter.UserViewHolder>() {

    override fun getItemCount() = data.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            UserViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false))

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

        var a:String = when (data[position].tod){
             "0" ->  "Ночь"
             "1" ->  "Утро"
             "2" ->  "День"
             "3" ->  "Вечер"
             else  -> "nach idi"
         }
         holder.date.text = data[position].toString() + "\n" + a
         holder.temp.text = data[position].temp.replace("&minus;", "-")
    }

    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var date = view.date_txt
        var temp = view.temp
    }

}