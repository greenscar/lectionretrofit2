package com.homekode.android.myapplication

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by ASUS on 05.02.2018.
 */
interface APIService {
    @GET("inf/meteo.php")
    fun getWeather(@Query("tid") a: Int): Call<List<Weather>>
}