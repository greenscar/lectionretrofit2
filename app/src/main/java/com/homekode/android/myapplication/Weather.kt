
package com.homekode.android.myapplication

import retrofit2.http.GET

/**
 * Created by ASUS on 05.02.2018.
 */
class Weather (var date: String,
               var tod: String,
               var pressure: String,
               var temp: String,
               var humidity: String,
               var wind: String,
               var cloud: String) {

     override fun toString(): String =
                     date + "\n" +
                     "Атмосферное давление: " + pressure + "\n" +
                             "Влажность воздуха: " + humidity + "\n" +
                     "Ветер: " + wind + "\n" +
                     cloud

}



